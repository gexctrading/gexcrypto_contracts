pragma solidity ^0.4.16;

import "./Owner.sol";
import "./SafeMath.sol";

contract GexCryptoPresale is owned {

    uint public presaleStart;
    uint public saleEnd;
    uint256 public presaleBonus;
    uint256 public buyingPrice;
    uint256 public totalInvestors;

    function GexCryptoPresale() {
        presaleStart = 1508011200; // 14 Oct
        saleEnd = 1512972000; // 11 Dec
        presaleBonus = 30;
        buyingPrice = 444444440000000; // 1 ETH = 2250 GEX
    }

    event EtherTransfer(address indexed _from,address indexed _to,uint256 _value);

    function changeTiming(uint _presaleStart,uint _saleEnd) onlyOwner {
        presaleStart = _presaleStart;
        saleEnd = _saleEnd;
    }

    function changeBonus(uint256 _presaleBonus) onlyOwner {
        presaleBonus = _presaleBonus;
    }

    function changeBuyingPrice(uint256 _buyingPrice) onlyOwner {
        buyingPrice = _buyingPrice;
    }

    function withdrawEther(address _account) onlyOwner payable returns (bool success) {
        require(_account.send(this.balance));

        EtherTransfer(this, _account, this.balance);
        return true;
    }

    function destroyContract() {
        if (msg.sender == owner) {
            selfdestruct(owner);
        }
    }

    function () payable {
        uint256 tokens = msg.value / buyingPrice;
        totalInvestors = totalInvestors + 1;

        if (presaleStart < now && saleEnd > now) {
            require(msg.value >= 1 ether);
        } else {
            revert();
        }
    }

}