pragma solidity ^0.4.16;

import "./Owner.sol";
import "./SafeMath.sol";

contract GexCryptoICO is owned {

    uint public stage1Start;
    uint public stage2Start;
    uint public stage3Start;
    uint public stage4Start;
    uint public saleEnd;

    uint256 public stage1Bonus;
    uint256 public stage2Bonus;
    uint256 public stage3Bonus;
    uint256 public stage4Bonus;

    uint256 public buyingPrice;
    uint256 public totalInvestors;

    function GexCryptoICO() {
        stage1Start = 1512972000; // 11 Dec
        stage2Start = 1513836000; // 21 Dec
        stage3Start = 1514786400; // 1 Jan
        stage4Start = 1516514400; // 21 Jan
        saleEnd = 1518328800; // 11 Feb

        stage1Bonus = 20;
        stage2Bonus = 15;
        stage3Bonus = 10;
        stage4Bonus = 0;

        buyingPrice = 465116270000000; // 1 ETH = 2150 GEX
    }

    event EtherTransfer(address indexed _from,address indexed _to,uint256 _value);

    function changeTiming(uint _stage1Start,uint _stage2Start,uint _stage3Start,uint _stage4Start,uint _saleEnd) onlyOwner {
        stage1Start = _stage1Start;
        stage2Start = _stage2Start;
        stage3Start = _stage3Start;
        stage4Start = _stage4Start;
        saleEnd = _saleEnd;
    }

    function changeBonus(uint256 _stage1Bonus,uint256 _stage2Bonus,uint256 _stage3Bonus,uint256 _stage4Bonus) onlyOwner {
        stage1Bonus = _stage1Bonus;
        stage2Bonus = _stage2Bonus;
        stage3Bonus = _stage3Bonus;
        stage4Bonus = _stage4Bonus;
    }

    function changeBuyingPrice(uint256 _buyingPrice) onlyOwner {
        buyingPrice = _buyingPrice;
    }

    function withdrawEther(address _account) onlyOwner payable returns (bool success) {
        require(_account.send(this.balance));

        EtherTransfer(this, _account, this.balance);
        return true;
    }

    function destroyContract() {
        if (msg.sender == owner) {
            selfdestruct(owner);
        }
    }

    function () payable {
        uint256 tokens = msg.value / buyingPrice;
        totalInvestors = totalInvestors + 1;

        if (stage1Start < now && saleEnd > now) {
            require(tokens >= 20);
        } else {
            revert();
        }
    }

}